
# Revision History
Revision history of SG Assestem

## Build ??? (To be Done)

1. [ ] Preset-commands (e.g. On/Off)
2. [ ] Log to file and GUI message clear.
3. [ ] Pre-run checking
4. [ ] Schema for file structure checking
   1. [ ] File: Template
   2. [ ] File: Proc
   3. [ ] File: Processor
   4. [ ] File: Instrument lib
   5. [ ] Global Params
   6. [ ] Step Parameter
5. [ ] (Better Prompt)Instrument model read-back: Check instrument model string.
6. [ ] Init Tables Function.
7. [ ] Peal: Syntax error report system for Peal
   1. [ ] Update Checking function
8.  [ ] Peal: bracket check during lexing.
9.  [ ] Use new dict model/view
10. [ ] Enable Copy/Paste
11. [ ] Cross-file merge
12. [ ] (Better Prompt)Add more message outputs
13. [ ] Confirmation: deleting table column
14. [ ] (Better Prompt)Check if channel is set when adding signal role(driver, tester, etc.)
15. [ ] Run expression code
16. [ ] Expression support assignment "<-" and "->" (R-style)
17. [ ] Store all editing widgets, enable undo/redo
18. [ ] Better clipboard support
19. [ ] When open a file, ask user if to reload inslib
20. 
21. [ ] Use `_id` as row header(Result table)
22. [ ] Add/fix positive ZCD function.
23. [ ] Double click to edit table cell, in new dialog
24. [ ] Table data plotting in new dialog.
25. [ ] Waveform Analyzer
26. [ ] Support for Exporting
27. [ ] Qt-native scope waveform viewer (in dialog)
28. [ ] Exporting: in-document link
29. [ ] generate issue list
30. [ ] Support power N6700B
31. [ ] Bug Fix: proc re-run bug.
32. [ ] When Running, No dumping/saving
33. [ ] Instruments Dock: Reload Button
    1.  [ ] reload connected
    2.  [ ] reload file and connected
    3.  [ ] re-search file, reload file and connected
34. [ ] Support chips with negative EN logic.
35. [ ] Sort before plotting
36. 
37. [ ] Scope plot in new dialog.
38. [ ] Scope measure display setting
39. [ ] Scope side bar display setting
40. [ ] When renaming table/signal/proc, check new name existence
41. [ ] Check if working data saved before open file.
42. [ ] Source in parallel (channel)
43. [ ] Source in parallel (instrument)
44. [ ] Multi-scope
45. [ ] Better Error out when:
    1.  [ ] Instrument Not Found
    2.  [ ] Signal Not Found
    3.  [ ] Param Not Found
46. [ ] Kikusui load CR SW
47. [ ] (Fenwick) Volt tolerance test(DC/Pulse)
50. [ ] ! For PNG screenshot for scope, filename-prefix shall be an expr.
51. [ ] !! R&S 10b/12b scope support
52. [ ] ! Break during sweeping when expr match.
54. [ ] ! Shorting Peak/Valley Measure: add delay(measure I_sw_p-p @{some time} after trig)
55. [ ] ! Manual select trig source in mild on/off, update doc.
56. [ ] !! Meter Aperture(PLC)/Z_input setting and not reset during measure. 
57. [ ] Voltage limit when setting voltage via closed-loop
58. [ ] !! COM-PORT as a instrument
59. [ ] Multi-channel
60. [ ] "I2C0" redirect to "I2C1"
61. [ ] Fast Saving


## Build 77? (Next Version)
1. Fix Keysight EXR scope "t_at_edge" measure
2. Fix DCDC_eval error while connecting to IO instrument
3. New step selector with filter
4. Add `Attachment` editor

## Build 769
1. Bugfix: all_ch_off of Keysight scope can't turn off CH4
2. Longer DP932/DP832 timeout.
3. Support SourceMeter Source Mode
4. Support Signal Generator




## Build 765
1. Add ADC Test (Developing)
2. Instrument Type: SourceMeter
3. Instrument: Keithley SourceMeter 2450
4. Instrument: Kikusui Power SourceMeter PBZ 20-20
5. Allow Expr in slew rate.
6. !!! Scope Measure code changed.

## Build 756
1. Support `len` PEAL function
2. Support UVLO-preventing in efficiency sweep
3. Support no-overlap table creating for `result_create_table`

## Build 755
1. Add scope meas_time: `x_at_max` `x_at_min` `x_at_edge_r` `x_at_edge_f`

## Build 754
1. Display Line number for the proc editor.
2. Doc error: mild V-in power-on doesn't need 2 srcs
3. Scope Read meas value(e.g. min/max)(for individual instr control steps). Meas function add params
4. Bugfix: Peal can not find variable when running efficiency sweep with multiple inputs and outputs(Signal redirection).
5. CH341 I2C bugfix: read_reg_8 use restart (not stop-then-start)
6. Bugfix: Right click instr resource view's empty space cause crashing
7. Bugfix: Crash when pasting unsupported data into list view (General Editor).
8. I2C CH431 supports continuous reading with restart (via "instr_io_i2c_read_after_write")
9. GUI update: Quick key copy (General StrMap Editor)
 
## Build 747
1. Peal: Bitwise/mod operations: & | ~ << >> %
2. Replace Stop icon, it was a 'record' icon.
3. Recursive dialog for value editing. supporting all types include list, dict, set, tuple; and custom types: ExternalFile, CheckResult etc.
4. Run from a middle step of a proc.
5. Model-View for step list (proc editor)
6. Bugfix: `load_auto_restart` won't work. now load only restarts when changing Range.
7. Support efficiency sweep with multiple inputs and outputs.

## Build 707
1. Only `DcDcEval_V03` is supported
2. Bug fix for set_ch of Prodigit load
3. Bug fix of pasting unsupported data to Proc_Step_List editor
4. Virtual instrument support.
5. Replace Stop icon, it was a 'record' icon.
6. Run from at-middle step of a proc.
7. Peal: support bit-wise operations: & | ~ ^ << >> 
8. Peal: mod operation: %

## Build 693
1. Better I2C support
2. More params in Expr
3. New structure of DCDCEval steps
4. slew_rate 9e+37
5. Better data list editor(with copy/paste)
6. Better data list editor(with multi selection and dragging)
7. Better step list editor(with copy/paste)
8. Better step list editor(with multi selection and dragging)
9. run multi steps

## Build 692
1. Scope display support 
   1. Labels
   2. Clear
   3. Annotations
   4. All CHs off
2. Dragging in list editor
3. I2C Support on SISCM2201A
4. Peal: Cancel "$"



## Build 690 (Assestem)
1. Basic Instrument control to GUI, With Peal parameter support
2. Bugfix: Scope DSOX4054A have bug when read back waveform data. (data length doesn;t match the header)
   1. A software patch is written.
3. Bugfix: Bad commands, including Counter measure freq. (not tested.)
4. Update basic(general) editor: 
   1. Support unknown types(read-only) and Boolean types.
   2. Better float editor
   3. Peal expression editor.
5. Scope label/tag support.
6. Data plot in a new dialog.
7. Step editor changed to general editor.
8. Bug fix: Set changed = true when eval param changed
9. Support inspecting I_out during load_tran


## Build 687
1. Table structure editing: Delete/Insert Columns
2. CLI open: file as arg, `.exe` file can be set as default app.
3. External file: Open With default app
4. External file: Reveal in explorer
5. Peal: support `[]` for generating List
6. Peal: support `[]` for subscription
7. Peal: support function calling
8. Peal: support `.` for getting obj's params
9.  QSplitter in main window and step editor
10. Highlighter on table filter
11. Pass/Fail Checker
12. Statistic/Coverage Checker
13. General list/dict editor
14. Let user to hide/show table columns.
15. Add code builder on column header
16. Support float: a.be+-c
17. List Supported instruments in help menu
18. enable Chip_ID
19. enable table_name_prefix/suffix

## Build 679
1. Brand-new table editor
2. Fix bug of DP832 channel mapping.
3. Peal: A expression parser for Table Filter

## Build 665
1. Independent SW freq Sweep
2. Independent Inductor current Sweep
3. Independent Dead time Sweep
4. Independent BST Cap/FB Voltage Sweep
5. Scope lib update: ACQ timeout relevant to timescale.
6. Report generator update: Filter
7. Report generator update: Description
8. Report generator update: Bug fix of subtitle
9. Report generator update: Add plotting params
10. Report generator update: Add `dual_param_table` section type
11. GUI tool added: Merge result table
12. GUI tool added: Result Table row delete

## Build 660 (Debug Version)
1. Vin very slow power on (via mild_power_on with supported src.)
2. Ven very slow power on (via mild_power_on with supported src.)
3. EN power-on supports inspecting V_en
4. Bugfix: Fix Scope ERROR via updating sync logic.
5. Support quiescent_input_current_sweep
6. Support peak/valley current limit test
7. V_en digital power-on
8. Change "DcDcEval_NG" to "DcDcEval", old version of DcDcEval is no longer supported.
9. [About Debug Version] Debug versions can run with a cmd console(black command window) as debug output.

## Build 659
1. Enable CR Load in multiple tests(Except load tran)
2. Keysight scope armed signal
3. Time base tuned for powering off/line tran down
4. Smart E-load range (Via out scale).
5. Scope timeout can be tuned via full-scale.
6. Support dialog input during running.

## Build 656
1. Bugfix: Chroma instrument class are not loaded during program start-up.

## Build 655
1. Tool Added: Folder Cleaner

## Build 653
1. Bugfix: Crash adding parameter
2. Bugfix: Crash connecting instruments when lib not set.
3. Turn off power source when step finished.
4. New parameter for `DcDcEval`: `board_I_in_max`. 
5. All params of `DcDcEval`:
   1. `envir_temperature_C`
   2. `board_V_en_max`
   3. `board_V_in_max`
   4. `board_set_V_out`: Influence scope horizontal scale, V_out vertical scale and V_out offset.
   5. `board_L_sw`: Influence scope horizontal scale and V_out vertical scale.
   6. `board_C_out`: Influence scope horizontal scale and V_out vertical scale.
   7. `board_C_in`
   8. `chip_I_sw_max`: Influence scope vertical scale of I_ind
   9. `board_I_in_max`: The current setting of V_in, not larger than power supply's limit.
6. All Supported instruments:
   1. `IOModRPiRemoteIO`: RPI
   2. `ScopeMDO4xxx`: TEK scope
   3. `PowerDH1766`: Dahua power
   4. `Power62xxxP`: Chroma power source
   5. `ELoadPlzxx5w`: Kikusui load
   6. `MMeter3441X`: Keysight
   7. `ScopeDSOX40x4`: Keysight
   8. `MMeter34461A`: Keysight
   9. `MMeter3441X`: Keysight
   10. `DataAcqDAQ970A`: Keysight
   11. `PowerE362xx`: Keysight

## Build 650
1. Bug fix: data sync when adding row to parameters
2. Bug fix: YAML Loader.

## Build 649
1. Bugfix: Exe do not load doc template file automatically.
2. New parameter for `DcDcEval`: `chip_I_sw_max`.
3. Measure peak/valley SW current when doing short recovery tests.

## Build 647
1. Start-up SCP trigger is set as argument.
2. Bugfix: Add breakpoint in function 'get_power_on_off_wfm'

## Build 646
1. Enable copying local instrument address to clipboard.
2. Abstract DC-Eload instr type.
3. When testing load-tran, The ripple scope scale can be controlled by user.
4. When testing power-on scp recovery, trigger at V_out 

## Build 642
1. Re-implemented vin_power_on_off_sweep
2. Update function documents.
3. "_runtime_issues" table is updated when triggering issue occurred.

## Build 641
Add sleep after ":SINGLE" command is sent.

## Build 640
Hotfix: range auto using mmeter to measure volt.  
[Need to be permanently fixed]

## Build 639
1. Bug Fix setting up wiring.

## Build 638
- New Function(s):
   1. Proc export/load
- New Instrument Support(s): 
   1. Power: Keysight E362XXA
   2. Power: Chroma 6xxxP
- Bug Fix(es):
   1. Data fetch error of scopes during manual capturing.
- Other Change(s):
   1. Use `*.inslib.yaml` instead of `*.inslib.yml` for instrument library files.



