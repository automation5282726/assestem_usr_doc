# Known Issues
## GUI(Assestem)
- [ ] SISCM2201 bug: I2C Read failed. Then always fail
- [ ] SISCM2201 Update: I2C R/W always return byte count
- [ ] IO module driver supports Read bytes.
- [ ] IO module hardware do not have to support reg operation.
- [ ] IO module hardware: 10 bits I2C。
- [ ] IO module hardware: SPI and 1-wire
- [ ] User defined waveform length
- [ ] New Painting logic.
- [ ] Loop Stress test.
- [ ] SEVERE Bug: Crash when call msg_call_func when it is None(A Eval obj just opened and did not run yet.) e.g. Editing signal, selected an un-connected instrument.
- [ ] SEVERE Bug: Crash when resource-tree's context menu is on, and tree is refreshed, then click action in menu.
- [ ] SEVERE Bug: When running all, no chance to save.
- [ ] SEVERE Bug: select_list() may cause min volt to to be filtered out. This should be done before running.
- [ ] Bug: pctrl_pulse_to_confirm do not have a string default.
- [ ] Bug: instr_load_set_ch is old. 
- [ ] Bug: load will restart every sweep step. (due to the "set_ch" function)
- [ ] Bug: [load will restart every sweep step] is fixed, and range goes wrong.
- [ ] User-defined signal name(for multi-channel DUT sweep)(Backend).
- [ ] User-defined signal name(for multi-channel DUT sweep)(Front end).
- [ ] Color match for scope waveforms.
- [x] CLI start-up
- [x] Copy local instrument address to clipboard.
- [ ] Ask user if reload inslib file when opening eval if found new.
- [ ] Duplicate proc.
- [ ] Duplicate result table
- [ ] Inspect/check all proc steps(parameters OK?)
- [x] Instrument lib auto-load
- [x] Args are displayed incorrectly when adding steps.
- [x] Output messages during uvlo/ven tests.
- [x] Sequence management of steps in proc is not implemented.
- [x] Tables are not supported when dumping to docx.
- [x] Pics in table cells are not supported when dumping to docx.
- [x] Should use dialog when dumping to docx.
- [x] Get right size in high-res screens.
- [x] Crashed when saving during running steps.
- [x] Confirm saving while newing.
- [ ] Should add more information on status bar.
  - [ ] Instrument lib path
  - [ ] Running status
  - [ ] File save/modify status
- [ ] Should use try-cath structure during opening and resolving files.
- [ ] Should use try-cath structure during opening testing instruments.
- [ ] Should use try-cath structure during data plotting.
- [ ] More information should be displayed in message list widget.
  - [ ] Exporting
  - [ ] Post-Processing
  - [ ] Opening 
  - [ ] Saving 
  - [ ] 
- [ ] Users have no chance to check wiring. Add wiring prompt before running steps.
- [x] Current design is not backward-compatible. Use general formats to store evaluation projects. 
  - [x] Solve circular reference inside the data structure.
- [x] Editing(Sync to obj) is not implemented.
  - [x] **IMPORTANT** Deleting result table
  - [x] Deleting result table-row
  - [x] Editing result
  - [x] Adding comment
  - [x] Adding proc
  - ...
- [x] Should check save status before exiting
- [ ] Should refresh the data tree when evaluation is running
- [x] Auto backup is not implemented.
- [ ] Should add multiple selection detect during instrument selection.
- [ ] Should add multiple selection detect during column selection (plotting).
- [ ] Editing instrument lib.
- [x] Should add help content
- [ ] Should add start-up page and better icon.
- [x] Clean-up folder: delete result files not in eval obj.

## Backend(evalcation)
- [x] Tek oscilloscope support: bugs when acq waveforms.
- [x] Bug Fix: Get IO voltage data from 6.5 digits meter.
- [x] Default message caller.
- [x] Design wire checking logic.
- [ ] Implement `meas_amplitude` of scopes.
  - [x] On keysight
  - [ ] On Tek
- [x] Use u/m when saving pic files.
- [x] Correct instrument types. Now some instruments are using compatible libs.
- [ ] Unsupported Evaluation Items
  - [x] V-en power on/off
  - [x] Hard Power on/off: Need Hardware Support
  - [x] Line Transient: Need Hardware Support
  - [x] Output Short tests
  - [ ] EC Table(Electrical Characteristics)
- [ ] **IMPORTANT** Failure/untested list for re-running is not implemented.
- [ ] Cross check during tests.(When applying settings to instr A, use B to read back and check.)
- [ ] Complete multi-scope support is not implemented.
- [ ] **IMPORTANT** Closed-loop instrument setting is not supported.
- [ ] **IMPORTANT** Add AC/DC and Bandwidth info to plots.
- [ ] Checking before running:
  - [ ] Parameters checking(Type)
  - [ ] Essential instruments checking(setup and connection)
  - [ ] Wiring test(Set-Read with DUT)
- [ ] Clear instrument status before running
- [ ] Use safe method to send stop flag to the running thread.
- [x] Instrument support: Keysight power 
- [x] Instrument support: Chroma power
- [x] **IMPORTANT** Instrument support: DAQ.
- [x] DUT support: DCDC Modules.
- [x] Hardware controller support: Raspberry PI
- [ ] Secondary checking before running single eval step.
  - [ ] Params checking
- [ ] Cleanup when running finished.
- [ ] Documentation





