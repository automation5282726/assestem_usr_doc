# Manual Operation
## Capturing Good Vertical Scale in Plot
Some scopes are 8-divs vertical. Thus you may set scope channel offset to $8*a*10^b$, where $a$ and $b$ are integers. Then the captured data plotted in 10-div vertical will have nice vertical scaling. For example, set offset to `3.2V`, `0.48A` etc. For screen vertical ratio, the 8-divs scopes should set to 0.125, 0.25, 0.375, 0.5, etc.

## Capturing Good Horizontal Scale in Plot
Editing...



# Recommended Procedures
## For Room Temperature


## For H/L Temperature
- (INIT STATUES)
  - Scope probes connected.
  - EN connected to power source channel.
- (DEVELOPING) Shutdown Input Current Sweep
  - EN = 0
- (DEVELOPING) No-load Input Current Sweep
  - Closed loop, EN to Vin
- (DEVELOPING) No-switching Input Current Sweep 
  - FB + high
- (MAN) Set EN High
- SW Detail Sweep (0.03A+)
- SW Detail Sweep No-load
- V_in Mild Power On/Off (0.03A+)
- V_in Mild Power On/Off No-Load
- (PULSE) ensure the BNC Cable is Connected
- Load Tran
- Short and recovery(via load tran)
  - Use Step: 
  - Params: Load CR;
- (PULSE) Short output
- Hiccup(via sw_detail)
  - Use Step: `run_sw_detail_sweep`
  - Params: Load CR;
- Startup SCP test
- (PULSE) Remove output shorting
- V_in UVLO(Skip this step if out is high)
- (PULSE) Connect EN to source
- V_en Hyst
- V_en mild power on
- (PULSE) Restore EN


# Put All Results in One File

## Merge(Under Develop)
Steps:
1. Run Procedures under condition A (V_out, T_ambient)
2. Change Board or Temperature
3. New a evaluation file(project) in new

## Changing Global Parameters

Steps:
1. Run Procedures under condition A (V_out, T_ambient)
2. Change Board or Temperature
3. Change global parameters.The following parameters should be changed and checked.  
  - `set_V_out`
  - `envir_T_amb_C` 
  - `board_L_sw` (If changed)
  - `board_C_out` (If changed)
3. Run procedures again under new condition like step 1, and continue.

# DO NOT Put Different Evaluation in the Same Folder
It is very dangerous if you put more than 1 evaluation file in the same directory. Because they are to share one `eval_result` dir, and the `Folder Cleaner` tool will delete all results from other evaluations.


# From Buck to Boost
- "sweep_filter_expr"
- I_ind scale
- V_sw scale


# Unsupported Evaluations
## Buck DC-DC
- EN de-glitch
- Min on/off time
- Temperature (TSD/RECOVERY)
- Temperature (Board IR Image)
- RDS-on
- Loop
