# Manual Operation
## Capturing Good Vertical Scale in Plot
Some scopes are 8-divs vertical. Thus you may set scope channel offset to $8*a*10^b$, where $a$ and $b$ are integers. Then the captured data plotted in 10-div vertical will have nice vertical scaling. For example, set offset to `3.2V`, `0.48A` etc. For screen vertical ratio, the 8-divs scopes should set to 0.125, 0.25, 0.375, 0.5, etc.

## Capturing Good Horizontal Scale in Plot
Editing...
