
# Instrument Library
Assestem needs information to control instruments. The information is located in a file, which has a filename with the suffix of `.inslib.yaml`.

The instrument lib file will be automatically loaded during program start-up if its full name is `instruments.inslib.yaml` and this file is under the same directory with the Assestem program file (`.exe`). 

!!! note

    Make sure that the instrument library file is properly set before controlling any instruments. 


## Typical Visa Instrument Types
Visa is a standard interface, commonly seen types of visa instruments are listed:
- `TCPIP0::192.168.2.100::inst0::INSTR` or `TCPIP::192.168.2.100::INSTR`: via LAN (VXI-11).
- `TCPIP0::192.168.2.100::hislip0`: via LAN (HiSLIP).
- `TCPIP0::192.168.2.100::5025::SOCKET`:via LAN (RawSocket).
- `GPIB0::20::INSTR`: GPIB Based
- `ASRL3::INSTR`: Serial Based
- `USB::0x0AAD::0x0119::022019943::INSTR`- via USB (USB-TMC)，Vendor ID 0xAAD，Product ID 0x119, Serial number 022019943。

!!! note
    Not all instruments supports Visa.

## Format
The instrument library file is a YAML file. The basic syntax for YAML can be seen at https://yaml.org/spec/1.2.0/#id2564746 .

### IP/USB Visa Instruments
This is the most common instrument type.
```yaml
KS Power:                           # Name, Unique, Arbitrary
    cls_model: PowerE362xx          # Type string
    cls_params:
        - TCPIP::10.4.64.105::INSTR # Visa Address
```

### Serial Visa Instruments
The setup is the same as the IP/USB Visa instruments. But note that the name is `ASRL{N}::INSTR`, For `{N}` is the number same as `COM{N}` in Windows `Device Manager`.

Instruments under this type:
- Prodigit load
- Chroma load with RS232 (Second param is baud rate)

### Other Instruments
DAHUA DH1766 Power's Ethernet RJ45 port does not support Visa. It is a Bare TCP Device:
```yaml
  DahuaPowerXX:
    cls_model: PowerDH1766
    cls_params:
        - "XXX.XXX.XXX.XXX" # IP
        - 5025              # Port
```
But if you use USB to connect this instrument, Use Visa:
```yaml
Dahua Power XXXXX:      # Dahua Power
  cls_model: PowerDH1766_Visa
  cls_params:             
      - USB0::XXXXXX::0::INSTR
```


- PiRemoteIO does not support Visa. It is a Bare TCP Device:
  ```yaml
  RPI:                      # Name
      cls_model: IOModRPiRemoteIO
      cls_params:
      - "XXX.XXX.XXX.XXX"   # IP
      - 8677                # Port
  ```

## Notes
- During editing, the supported instruments can be found in Help menu.  
  ![](20221018093018.png)  

- Address for USB Visa devices can be copied from the Instruments dock window on the right.  
  ![](20221018092646.png)  


## File Example

```yaml
#### instruments.inslib.yaml ####

Main Power NET:
    cls_model: PowerE362xx
    cls_params:
        - TCPIP::10.4.64.XXX::INSTR

Meter NET:
    cls_model: MMeter34461A
    cls_params:
       - TCPIP::10.4.64.XXX::INSTR

Main Load NET:
    cls_model: ELoadPlzxx5w
    cls_params:
        - TCPIP::10.4.64.XXX::INSTR

```



