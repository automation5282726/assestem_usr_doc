
DCDC Evaluator is the basic evaluator for DC-DC power supply.

# Functions(Steps)





# Default Step Arguments
- load_auto_restart: BOOL. This controls the load to restart before every sweeping point. Setting this argument `True` is to force the DUT to quit the protection mode and output power. For DUTs that can not hold fast load transients, this should be set to `False`. Note that according to the load's manual, switching load range will require load off. The load will still be off when crossing ranges even `load_auto_restart` is set false.


# Default Signals


# Default Global Parameters
