# Eval VM
Evaluation Virtual Machine is the test step runner. It manages the evaluation session. When you start to run a procedure, it is called step by step by the Eval VM. The VM is Turing complete. Which means, you can program via the step runner.

## Params/Context


### Auto-set
IN DEVELOPMENT

- `__last_result_row__`
- `__last_result_key__`
- `__result_table_name_suffix__` = ''  If set, auto add to table_name

### Virtual
IN DEVELOPMENT

- `__stop_flag__`
- `__error_flag__`
- `__start_time__`
- `__time_now__`

# Statements (Peal)
## Context

See `evalcation/emql/context_presets`


