
## File Exporting
### Export to MS docx Report
  - Right Click Evaluation Object in the resource window.
  - Select a document template file
  - Export .DOCX file
### Export Data to csv Files
  - Click a result table on the resource window
  - Select data to export in the content tag
  - Click button to export in the content tag


