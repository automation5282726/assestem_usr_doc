
# Quick Start
This section offers the basic operations, step by step.


## Setting-up Environment
- If you want to open an existing evaluation file (`.tbev`) only (For example, examining the evaluation results), you needs nothing but the program file (`.exe`)
- If you want to run evaluation via your instruments, you needs an `.inslib.yaml` file containing the instrument information.
  - See guide for instrument library for more information.
- If you want to run an existing procedure, you need a `.evproc.yaml` file with pre-defined steps.
- If you want to run post-processing after the data (Evaluation results) is gathered, you need a `.resproc.yaml` file.
- If you want to export your result data to a `.docx` MS Word document, you needs a `.doct.yaml` template file.


## Starting a New Evaluation
1. Click `File -> New` for new evaluation.
2. Finish the wizard with parameters properly set. For example, the new evaluation is named `SGM01`, and parameter "board_V_out_set" is set to 3.3V.
3. Click `SGM01 -> procs -> default` in the left-side tree widget(Resource dock window).
4. Add step, edit parameters in the central widget(Content tag).
5. You can change the project folder name or evaluation file name as you wish, even though they are created with the same name of the evaluation.


## Opening an Existing Evaluation File
If you have a Evaluation file, you can open it instead of Newing one. 
1. Click `File -> Open` to open an existing evaluation file.

## Editing a Procedure
You can find procedure list in the left tree widget(Resource Dock)
1. If there is no procedure in the procedure list, new one. (Find item after right-clicking)
2. Add/Del/Move steps in the procedure.
3. Edit step parameters.
4. Save file after editing.

## Running Steps
1. Select a procedure in the resource window.
2. Select a step in the content tag.
3. Make sure the instrument library is loaded.
4. Click `Run all steps` to run the whole procedure or click `Run selected steps` to run selected steps only.
5. Select instruments you are using.
6. Then, the evaluation will be started. To store result, table(s) will be generated.


## Checking Results
1. Wait for the running procedure to finish.
2. Find "results" section in the tree view(left of the window by default)
3. Expand and click the generated table, you will see the results.

!!! note
    
    if you can not find the result of the newly run step, try to refresh the tree (by right clicking the evaluation root and click refresh)


