# User Guide
[TOC]

## Result Table
### Special Table names
- `_runtime_issues` Store run-time issues raised in integrated steps.
- `_result_issues`
- `_pivot_tables`

### Special Table Columns
- `_id` Unique ID of rows in table. A integer number.
- `_assessment_manual` Default assessment row.
- `_assessment_auto` Default assessment row.

### Coverage Table (Added)
- `__worst_issue` For coverage table. This indicates the worst issue level of the row.
- `__table_name` This is for coverage table. Since the data from coverage table is from different tables, this column is to tell which table is the data row from


# File Types
- `*.bev` and `*.tbev`: Main file(Evaluation file). It is recommended to save as `tbev` file.
  - `bev`: Board evaluation (recommended).
  - `tbev`: Text board evaluation (obsolete).
- `*.evproc.yaml`: Evaluation Procedure Export
- `*.resproc.yaml`: Result Table Processor
- `*.inslib.yaml`: Instrument Library
- `*.doct.yaml`: Document Export Template



# Variable/Table Cell/Parameter Types


## Global Parameters
- int, float, string
- list, 
- dict(key is string)
- Expression(in dev)


## Step Arguments
- int, float, string
- list, 
- dict(key is string) (in dev)
- Expression (in dev)


## Table Cells
- int, float, string
- list, 
- dict(key is string) (in dev)
- * Expression (in dev)
- External File
- Row Status


# Default Units

- Current       (I): A
- Voltage       (U): V
- Inductance    (L): H
- Capacitance   (C): F
- Resistance    (R): Ohm, Ω
- Conductance   (G): S
- Temperature   (T): degC, ℃


# Using Peal Statements
The statement system (Evaluation Management Query Language, Peal) in this software is a tiny tool based on user codes.
Statements can be used for:
- Checking pass/fail after running
- Skip/select conditions(points) when running
- Filter table rows when viewing at GUI (will not change the eval file)
- Filter table rows when exporting document

## Rules
- No opcode without operand on the right. for example, "a++" is now not supported.
- Single quote `'` is not used.
- Bitwise logic opcodes `^` `~` `|` `&` are not supported.
- `(XX, XX, )` last-comma is not supported in bracket.
- `` ` `` is not supported.
- Float Numbers:
  - Rules:
    - No Space inside.
    - Starts with a number (0-9).
    - Syntax: `a[.b][{e,E}[{+,-}]c]`, a, b, c are integer numbers.
  - Examples:
    - BAD: `.3`, `e-3`, `e3`, `7E +5`
    - GOOD: `2e-4`, `3.3`, `0.4e-6`, `7E+5`, `7e5`, `7E5`


## Special Operations
- `%` with floating numbers
- `**`: Power
- `//`: division returns integers
- `?:` condition operator is same as Clang (right-to-left):
  - `$a ? $b ? $c : $d : $e` == `$a ? ($b ? $c : $d) : $e` (Middle)
  - `$a ? $b : $c ? $d : $e` == `$a ? $b : ($c ? $d : $e)` (Right)
  - `($a ? $b : $c) ? $d : $e` as it is.

## Variable Access
User can use `$` to access variables.
- For run-time operations, it refers to global parameters and any-d(Any-Dimensional) params for the current step(iteration).
- For post-run operations, it refers to the table column name.

## Examples

- Display Heavy-Load Data Only: `$set_I_out>1.5`
- Two-level select: `($set_V_in < 3 && $set_V_in * $meas_I_in >5.5) || ($set_V_in >= 3 && $set_V_in * $meas_I_in >4.5)`
- Multi-level select: 
  ```
  $set_I_out<0.1?$meas_V_ripple>0.8:
  $set_I_out<1.0?$meas_V_ripple>1.0:
  $set_I_out<6.0?$meas_V_ripple>1.5:
  $meas_V_ripple>2
  ```


# Dealing With Result
The results are stored in tables. 

At lower level, almost every Python object can be put in the table cell.
However, There are only several types are supported by the GUI:
- Basic Types `int`, `str`, `float`
- `AttachmentObj` Can not be created in table GUI
- `AssessmentObj` Can not be created in table GUI



# About The Attachment Files

A evaluation file will have attachments stored in `AttachmentObj` Objects. The Files in the result folder bot cen not be found in tables will be regarded as garbage and can be cleaned-up by the folder cleaning tool. So it is  **Dangerous** to share the same folder with more than one Evaluation projects , because attachments of other project will be deleted if you opened one eval and ran the cleaning tool.




# Notes For Instrument Channel Usage

For each role of a signal (e.g. `DC_TESTER` for `V_in`), only one signal:channel is used. 
Although you can put multiple `DC_TESTER` to a same signal, generally, only the top one is used. There are exceptions like efficiency testing(you can manually change to meter's 10A port)

## Measure Type Defined for Scopes


## Channel Multiplex
A channel of a instrument can be attached to several different signals. 
This relies on manual operations to re-connect cables(probes).
For example, Scope CH1 is connected to I_out _when testing load_tran while "inspect_i_out" is on, The program will set the I_out's waveform inspector channel as "I_out", and make waveform with tag "I_out". In most cases, another signal will not be captured due to the limited channel number.



